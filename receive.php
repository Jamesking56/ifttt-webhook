<?php

/**
 * IFTTT Wake-on-lan by James King.
 * For more information, see .
 */

use Ssh\Session;

require_once('config.php');
require_once('vendor/autoload.php');

$configuration = new Ssh\Configuration($host);
$authentication = new Ssh\Authentication\Password($username, $password);

$session = new Session($configuration, $authentication);

$exec = $session->getExec();

$dateTime = new DateTime();
file_put_contents('ifttt.log', 'IFTTT WakePC ran on '.$dateTime->format('d/m/y H:i:s')."
", FILE_APPEND);

echo $exec->run($command);
